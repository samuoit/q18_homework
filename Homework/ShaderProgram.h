#pragma once
#include "GL\glew.h"

#include <MiniMath\Core.h>
#include <string>

class ShaderProgram
{
public:

	ShaderProgram();
	~ShaderProgram();

	// Load a vertex shader and a fragment shader, and places them in a program
	bool Load(const std::string &vertFile, const std::string &fragFile);
	bool isLoaded() const;
	// Clear all data from OpenGL
	void UnLoad();
	bool LinkProgram();	// Allows to re-link shader program

	// Makes / Cancels shader as a current shader
	void Bind() const;
	void UnBind();

	// Requires a re-link before OpenGL will register the change
	void AddAttribute(unsigned index, const std::string &attribName);

	// Returns -1 if the attribute does not exist
	int GetAttribLocation(const std::string &attribName);
	// Returns -1 if uniform does not exist
	int GetUniformLocation(const std::string &uniformName);

	// Send data to shader
	void SendUniform(const std::string &name, int integer);
	void SendUniform(const std::string &name, unsigned int unsigned_integer);
	void SendUniform(const std::string &name, float scalar);
	void SendUniform(const std::string &name, const vec2 &);
	void SendUniform(const std::string &name, const vec3 &);
	void SendUniform(const std::string &name, const vec4 &);
	void SendUniformMat3(const std::string &name, float *matrix, bool transpose);	// For MiniMath we need transpose
	void SendUniformMat4(const std::string &name, float *matrix, bool transpose);
	
private:
	bool _IsInit = false;
	GLuint _VertexShader = 0;
	GLuint _FragShader = 0;
	GLuint _Program = 0;

	std::string ReadFile(const std::string &fileName) const;
	bool CompileShader(GLuint shader) const;
	void OutputShaderLog(GLuint shader) const;
	void OutputProgramLog() const;


};

