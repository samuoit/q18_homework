#pragma once
#include <windows.h>

#include "ShaderProgram.h"
#include "Mesh.h"
#include "Texture.h"

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include "Timer.h"

#define WINDOW_WIDTH			800
#define WINDOW_HEIGHT			600
#define FRAMES_PER_SECOND		60

enum VIEWER_ACTIVE
{
	VIEWER_CAMERA = 0,	// Scene viewed from camera POV
	VIEWER_LIGHT = 1,	// Scene viewed from light POV
	VIEWER_OTHER = 2	// Scene viewed from other POV
};

enum MOVE_DIRECTION
{
	MOVE_UP = 1,		// Sphere should move up
	MOVE_DOWN = -1		// Sphere should move down
};

class Game
{
public:
	Game();
	~Game();

	void initializeGame();
	void update();
	void draw();

	void Game::drawObject			// Bind shader program, send uniforms, and draw mesh object (TODO: implement set of uniforms like array)
	(
		//ShaderProgram &program,	// Shader program to use
		Mesh &obj,					// Mesh object
		Texture &tex,				// Object texture
		mat4 &objTransform			// Object transform
	);

	/* input callback functions */
	void keyboardDown(unsigned char key, int mouseX, int mouseY);
	void keyboardUp(unsigned char key, int mouseX, int mouseY);
	void mouseClicked(int button, int state, int x, int y);
	void mouseMoved(int x, int y);

	/* Data Members */
	Timer *updateTimer	= nullptr;
	float TotalGameTime = 0.0f;

	// We now use objects we created in previous classes
	ShaderProgram progSimpleLighting;
	Mesh objFloor;
	Mesh objSphere;
	Mesh objTorus;
	Mesh objMonkey;

	// Texture objects for all models
	Texture texFloor;
	Texture texSphere;
	Texture texTorus;
	Texture texMonkey;

	// Camera, and light transforms
	mat4 camProjection;			// Camera projection matrix
	mat4 camTransform;			// Camera postion and transforms
	mat4 lightProjection;		// Light projection matrix
	mat4 lightTransform;		// Light position and transforms
	mat4 otherTransform;		// Other position and transforms
	
	// Object transforms
	mat4 floorTransform;		// Floor object position and transformation
	mat4 sphereTransform;		// Sphere object position and transformation
	mat4 torusTransform;		// Torus object position and transformation
	mat4 monkeyTransform;		// Monkey object position and transformation

	// Active point of view
	VIEWER_ACTIVE activeViewer;	// Active scene viewer i.e. Point of View (POV)

	// Direction in which sphere should be moving
	MOVE_DIRECTION torusMoveDirection;

private:

};
