#version 430

// -- Lighting
uniform vec3 uLightPosition;			// Position of light in the world
uniform vec3 uLightAmbient;				// Abmient light
uniform vec3 uLightDiffuse;				// Diffuse light
uniform vec3 uLightSpecular;			// Specular light

uniform float uLightSpecularShininess;	// Shinines
uniform float uAttenuation_Constant;	// Constant falloff
uniform float uAttenuation_Linear;		// Linear falloff
uniform float uAttenuation_Quadratic;	// Quadratic falloff
uniform sampler2D uTex;					// Object texture

// Input data
in vertData
{
	vec3 position;		// Position of vertex in a model-view space
	vec2 texcoord;		// Texture coordinates
	vec3 normal;		// Normals needed for lighting calculations	
} vIn;

out vec4 FragColor;		// Output from fragment shader

void main()
{
	FragColor.rgb = uLightAmbient;						// Final color will have contribution of ambient light
	
	vec3 norm = normalize(vIn.normal);					// Normalize normal, as there was some rasterizer interrpolation
	vec3 lightVec = uLightPosition - vIn.position;		// Calculate light vector based on light position and vertex where light hits
	float dist = length(lightVec);						// Calculate distance of light i.e. light vector magnitude
	vec3 lightDir = lightVec / dist;					// Calculate light direction

	
	// Check if light is hitting normal or is paralel with it (dot product 0 means they are paralel)
	float NdotL = dot(norm, lightDir);

	// If normal and light are not paralel - the light contributes to this surface
	if (NdotL > 0.0)
	{
		// Calculate total attenuation (falloff) - One (1) over (constant attenuation + linear depending on distance + quadratic based on distance squared)
		float attenuation = 1.0f / (uAttenuation_Constant + (uAttenuation_Linear * dist) + (uAttenuation_Quadratic * dist * dist));

		// Calculate diffuse lighting contribution after falloff
		FragColor.rgb += uLightDiffuse * NdotL * attenuation;

		// Bling-Phong half vector
		float NdotHV = max(dot(norm, normalize(lightDir + normalize(-vIn.position))), 0.0f);

		// Calculate specular lighting contribution
		FragColor.rgb += uLightSpecular * pow(NdotHV, uLightSpecularShininess) * attenuation;	// Even specular light has falloff
	}

	// Sample texture and apply light contributions to it
	vec4 textureColor = texture(uTex, vIn.texcoord);

	FragColor.rgb *= textureColor.rgb;					// Light affects rgb, but not alpha
	FragColor.a = textureColor.a;						// Alpha is not affected by light

}