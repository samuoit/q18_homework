#version 430

layout(location = 0) in vec3 posIn;
layout(location = 1) in vec2 uvIn;
layout(location = 2) in vec3 normalIn;

uniform mat4 uModel;
uniform mat4 uView;
uniform mat4 uProj;

//out vec2 texcoord;
//out vec3 norm;	// normals for lighting
//out vec3 pos;

out vertData
{
	vec3 position;	// transformed vertex position
	vec2 texcoord;	// texture coordinate
	vec3 normal;	// normal for lighting
} vOut;

void main()
{
	vOut.texcoord = uvIn;
	vOut.normal = mat3(uView) * mat3(uModel) * normalIn;

	vec4 viewSpace = uView * uModel * vec4(posIn, 1.0f);

	gl_Position = uProj * viewSpace;
	
	vOut.position = viewSpace.xyz;
}