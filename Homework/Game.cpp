#include "Game.h"

float deltaTime;

float angle = 0.0f,

deltaAngle = 0.0f;
int xOrigin = -1;
float mouseCurrentX = 0.0f, mouseCurrentY = 0.0f, mouseCurrentZ = 0.0f;

Game::Game(){}

Game::~Game()
{
	delete updateTimer;
	progSimpleLighting.UnLoad();
	objMonkey.Unload();
	texMonkey.Unload();
}

// Distance between two points with equal Y coordinate
float getDistance(vec3 &pointOne, vec3 &pointTwo)
{
	float xDistance = pointOne.x - pointTwo.x;
	float zDistance = pointOne.z - pointTwo.z;
	
	return sqrt(xDistance * xDistance + zDistance * zDistance);
}

// Translate object orbitally by increment for object in XZ plane around the Y axis of any specified object (center)
void RotateInOrbit(mat4* rotatingObjectTransform, vec3 centerOfRotationInXZPlane, float angleIncrement)
{
	vec3 orbitalIcnrement;

	// Get current positon of the object
	vec3 currObjPosition = (*rotatingObjectTransform).GetPositionVec();
	
	// Calculate diagonal distance to current rotating object's center from rotation origin
	float radius = getDistance(currObjPosition, centerOfRotationInXZPlane);
	
	// Calculate Z distance between rotating object's center and rotation origin
	float zDistance = abs(currObjPosition.z - centerOfRotationInXZPlane.z);

	float currentAngle = acosf(zDistance / radius);		// Get current angle between Z axis and object's position
	currentAngle += angleIncrement;						// Increment angle by specified increment
	while (currentAngle >= 360) currentAngle -= 360;	// Reduce angle if higher than 360 (full circle) - result will be the same
	
	printf("\n\nCos: %.2f , Sin: %.2f , Distance to Point: %.2f , Projection Distance: %.2f", cos(ToRadians(currentAngle)), sin(ToRadians(currentAngle)), radius, zDistance);

	orbitalIcnrement.x = currObjPosition.x + cos(ToRadians(currentAngle)) * radius;		// Calculate and set new X coordinate increment
	orbitalIcnrement.z = currObjPosition.z + sin(ToRadians(currentAngle)) * radius;		// Calculate and set new Z coordinate increment

	printf("\nRadius: %.2f, Current Angle: %f.2 , New Position %.2f %.2f %.2f\n", radius, currentAngle, orbitalIcnrement.x, orbitalIcnrement.y, orbitalIcnrement.z);

	(*rotatingObjectTransform).Translate(orbitalIcnrement.x, currObjPosition.y, orbitalIcnrement.z);
}

// Calculate normals for vertices (e.g. if missing from .obj file)
vec3 calcNormal(const vec3 &p1, const vec3 &p2, const vec3 &p3)
{
	vec3 V1 = (p2 - p1);
	vec3 V2 = (p3 - p1);
	vec3 surfaceNormal;
	surfaceNormal.x = (V1.y * V2.z) - (V1.z - V2.y);
	surfaceNormal.y = -((V2.z * V1.x) - (V2.x * V1.z));
	surfaceNormal.z = (V1.x * V2.y) - (V1.y * V2.x);

	// Dont forget to normalize if needed
	return surfaceNormal;
}

void DisplayKeysInfo(const std::string before_message = "", const std::string after_message = "")
{
	if (before_message.size() > 0) printf("\n %s \n", before_message.c_str());
	// Display keys used to switch between views
	printf("\nSwitch Active Viewer: \n'K' to view from camera's point of view\n'L' to view from light's pov\n");
	printf("\n-------------------------------------------\n");
	printf("\nMove active viewer:\n");
	printf("\n-------------------------------------------\n");
	printf("\nW - move forward");
	printf("\nS - move back");
	printf("\nA - move left");
	printf("\nD - move right");
	printf("\nZ - rotate left around Y");
	printf("\nX - rotate right around Y");
	printf("\nR - move up");
	printf("\nF - move down");
	printf("\n\n");
	if (after_message.size() > 0) printf("\n %s \n", after_message.c_str());
}

void Game::initializeGame()
{
	//// Pyramid points
	//vec3 p1 = vec3(0.000000, -0.353553, -0.707107);
	//vec3 p2 = vec3(-0.707107, -0.353553, -0.000000);
	//vec3 p3 = vec3(-0.000000, -0.353553, 0.707107);
	//vec3 p4 = vec3(0.707107, -0.353553, 0.000000);
	//vec3 p5 = vec3(0.000000, 0.353553, 0.000000);

	//// Pyramid normals
	//printf("vn %f %f %f", calcNormal(p1, p4, p2).x, calcNormal(p1, p4, p2).y, calcNormal(p1, p4, p2).z);
	//printf("vn %f %f %f", calcNormal(p2, p4, p3).x, calcNormal(p2, p4, p3).y, calcNormal(p2, p4, p3).z);
	//printf("vn %f %f %f", calcNormal(p1, p2, p5).x, calcNormal(p1, p2, p5).y, calcNormal(p1, p2, p5).z);
	//printf("vn %f %f %f", calcNormal(p2, p3, p5).x, calcNormal(p2, p3, p5).y, calcNormal(p2, p3, p5).z);
	//printf("vn %f %f %f", calcNormal(p3, p4, p5).x, calcNormal(p3, p4, p5).y, calcNormal(p3, p4, p5).z);
	//printf("vn %f %f %f", calcNormal(p4, p1, p5).x, calcNormal(p4, p1, p5).y, calcNormal(p4, p1, p5).z);

	updateTimer = new Timer();		// Start timer

	glEnable(GL_DEPTH_TEST);		// Enable depth testing

	// Initialize shader program with shaders to light the scene
	if (!progSimpleLighting.Load("./Assets/Shaders/Passthrough.vert", "./Assets/Shaders/Passthrough.frag")) printf("Shader program failed to initalize.\n");
	
	// Load object file (monkey)
	if (!objFloor.LoadFromFile("./Assets/Models/Cuboid.obj")) printf("Floor (cuboid) model failed to load.\n");
	if (!objSphere.LoadFromFile("./Assets/Models/Sphere.obj")) printf("Sphere model failed to load.\n");
	if (!objTorus.LoadFromFile("./Assets/Models/Torus.obj")) printf("Torus model failed to load.\n");
	if (!objMonkey.LoadFromFile("./Assets/Models/Pyramid.obj")) printf("Monkey model failed to load.\n");
	// Monkey
	
	// Load textures
	if (!texFloor.Load("./Assets/Textures/Floor_min.png")) printf("Floor texture failed to load.\n");
	if (!texSphere.Load("./Assets/Textures/4096_earth.jpg")) printf("Sphere (earth) texture failed to load.\n");
	if (!texTorus.Load("./Assets/Textures/Torus.png")) printf("Torus texture failed to load.\n");
	if (!texMonkey.Load("./Assets/Textures/Fur.png")) printf("Fur (monkey) texture failed to load.\n");

	// Create projection matrix (can be used for all POVs)
	camProjection.FrustumProjection(60.0f, (float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, 1.0f, 10000.0f);
	lightProjection.FrustumProjection(90.0f, (float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, 1.0f, 1000.0f);

	// Move camera to world coordinate system
	camTransform.Translate(0.0f, 0.0f, 5.0f);

	// Move light to world coordinate system
	lightTransform.Translate(2.0f, 5.0f, 1.0f);
	
	// Move objects to world coordinate system
	floorTransform.Translate(0.0f, -3.0f, -5.0f);
	sphereTransform.Translate(-3.0f, 0.0f, -1.0f);
	torusTransform.Translate(3.0f, 0.0f, -4.0f);
	monkeyTransform.Translate(-2.0f, 1.0f, -7.0f);

	// Scale floor
	floorTransform.Scale(10.0f, 0.01f, 10.0f);

	// Initial sphere moving direction (up)
	torusMoveDirection = MOVE_UP;

	// Set active viewer, and display keys used to switch between viewers
	activeViewer = VIEWER_CAMERA;	// Initially scene viewed from camera POV
	DisplayKeysInfo("==== Currently Scene Viewed from Camera's POV ===", "-----------------------------------------------------");
}

void Game::update()
{
	// Update clock to get time since the last update
	updateTimer->tick();

	// Calculate time from last update
	deltaTime = updateTimer->getElapsedTimeSeconds();
	TotalGameTime += deltaTime;

	// Rotate monkey object
	monkeyTransform.RotateY(deltaTime * 45.0f);

	// Move up and down sphere object (also rotate around Y)
	if (torusTransform.GetPositionVec().y > 2.0f) torusMoveDirection = MOVE_DOWN;
	else if (torusTransform.GetPositionVec().y < -2.0f) torusMoveDirection = MOVE_UP;
	
	// Rotate torus around X, Y and Z axis
	torusTransform.RotateX(deltaTime * 15.0f);
	torusTransform.RotateY(deltaTime * 45.0f);
	torusTransform.RotateZ(deltaTime * 15.0f);

	// printf("\nSphere Position: %.2f %.2f %.2f", sphereTransform.GetPositionVec().x, sphereTransform.GetPositionVec().y, sphereTransform.GetPositionVec().z);

	// Rotate sphere in its own place
	sphereTransform.RotateY(deltaTime * 30.0f);

	// Rotate sphere in orbit around custom center (Y coordinates are ignored no matter what value is set)
	// RotateInOrbit(&sphereTransform, vec3(3.0f, 0.0f, -3.0f), deltaTime * 15.0f);
	torusTransform.Translate(0.0f, deltaTime * 2.0f * torusMoveDirection, 0.0f);

	//if (VIEWER_CAMERA == activeViewer) camTransform.Translate(vec3(mouseCurrentX, mouseCurrentY, mouseCurrentZ));

	
}

void Game::drawObject(Mesh &obj, Texture &tex, mat4 &objTransform)
{
	// Bind shader program and send uniforms
	progSimpleLighting.Bind();
	progSimpleLighting.SendUniformMat4("uModel", objTransform.data, true);

	if (VIEWER_CAMERA == activeViewer)
	{
		progSimpleLighting.SendUniformMat4("uProj", camProjection.data, true);
		progSimpleLighting.SendUniformMat4("uView", camTransform.GetInverse().data, true);
	}
	else if (VIEWER_LIGHT == activeViewer)
	{
		progSimpleLighting.SendUniformMat4("uProj", lightProjection.data, true);
		progSimpleLighting.SendUniformMat4("uView", lightTransform.GetInverse().data, true);
	}
	else 
	{
		progSimpleLighting.SendUniformMat4("uProj", camProjection.data, true);
		progSimpleLighting.SendUniformMat4("uView", otherTransform.GetInverse().data, true);
	}
	progSimpleLighting.SendUniform("uTex", 0);
	progSimpleLighting.SendUniform("uLightPosition", lightTransform.GetPositionVec());
	progSimpleLighting.SendUniform("uLightAmbient", vec3(0.89f, 0.80f, 0.30f));
	progSimpleLighting.SendUniform("uLightDiffuse", vec3(0.93f, 0.66f, 0.67f));
	progSimpleLighting.SendUniform("uLightSpecular", vec3(0.78f, 0.03f, 0.46f));
	progSimpleLighting.SendUniform("uLightSpecularShininess", 50.0f);
	progSimpleLighting.SendUniform("uAttenuation_Constant", 1.0f);
	progSimpleLighting.SendUniform("uAttenuation_Linear", 0.1f);
	progSimpleLighting.SendUniform("uAttenuation_Quadratic", 0.01f);

	// Bind fur (monkey) texture
	tex.Bind();

	// Draw monkey object using VAO
	glBindVertexArray(obj.VAO);
	glDrawArrays(GL_TRIANGLES, 0, obj.GetNumVertices());
	glBindVertexArray(0);

	// Unbind texture
	tex.Unbind();

	// Unbind shader program
	progSimpleLighting.UnBind();
}

void Game::draw()
{
	// Set clear color and what to clear
	glClearColor(0, 0, 0, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	drawObject(objFloor, texFloor, floorTransform);
	drawObject(objMonkey, texMonkey, monkeyTransform);
	drawObject(objTorus, texTorus, torusTransform);
	drawObject(objSphere, texSphere, sphereTransform);

	// Swap front and end buffers
	glutSwapBuffers();
}

void Game::keyboardDown(unsigned char key, int mouseX, int mouseY)
{
	// Matrix 4x4 representing transform for POV (camera, or light) 	
	mat4* povTransform = nullptr;

	// Set pointer to proper POV transform
	if (VIEWER_CAMERA == activeViewer) povTransform = &camTransform;			// Point to camera transform
	else if (VIEWER_LIGHT == activeViewer) povTransform = &lightTransform;		// Point to light transform
	else povTransform = &otherTransform;										// Point to any other defined transform

	// Calculate increment
	float moveIncrement = deltaTime * 10;
	float angleIncrement = deltaTime * 30.0f;

	switch(key)
	{
	case 'a':	// left
		(*povTransform).Translate(-moveIncrement, 0.0f, 0.0f);
		break;
	case 'd':	// right
		(*povTransform).Translate(moveIncrement, 0.0f, 0.0f);
		break;
	case 'w':	// forward
		(*povTransform).Translate(0.0f, 0.0f, -moveIncrement);
		break;
	case 's':	// back
		(*povTransform).Translate(0.0f, 0.0f, moveIncrement);
		break;
	case 'r':
		(*povTransform).Translate(0.0f, moveIncrement, 0.0f);
		break;
	case 'f':
		(*povTransform).Translate(0.0f, -moveIncrement, 0.0f);
		break;
	case 'x':	// rotate left
		(*povTransform).RotateY(angleIncrement);
		break;
	case 'z':	// rotate right
		(*povTransform).RotateY(-angleIncrement);
		break;
	case 'k':
		activeViewer = VIEWER_CAMERA;
		DisplayKeysInfo("==== Currently Scene Viewed from Camera's POV ===", "-----------------------------------------------------");
		break;
	case 'l':
		activeViewer = VIEWER_LIGHT;
		DisplayKeysInfo("==== Currently Scene Viewed from Light's POV ===", "-----------------------------------------------------");
		break;
	case 'o':
		activeViewer = VIEWER_OTHER;
		DisplayKeysInfo();
		break;
	case 27: // the escape key
	case 'q': // the 'q' key
		exit(1);
		break;
	}
}

void Game::keyboardUp(unsigned char key, int mouseX, int mouseY)
{
	switch(key)
	{
	case 32: // the space bar
		break;
	case 27: // the escape key
	case 'q': // the 'q' key
		exit(1);
		break;
	}
}

void Game::mouseClicked(int button, int state, int x, int y)
{
	if(state == GLUT_DOWN) 
	{
		switch(button)
		{
		case GLUT_LEFT_BUTTON:
			angle -= deltaAngle;
			xOrigin = -1;
			break;
		case GLUT_RIGHT_BUTTON:
		
			break;
		case GLUT_MIDDLE_BUTTON:

			break;
		}
	}
	else
	{
		xOrigin = x;
	}
}

void Game::mouseMoved(int x, int y)
{
	if (xOrigin >= 0) deltaAngle = (x - xOrigin) * 0.001f;

	mouseCurrentX = sin(angle - deltaAngle);
	mouseCurrentY = -cos(angle - deltaAngle);

	/*
	if (mouseCurrentX > 0 && mouseCurrentX < WINDOW_WIDTH) mouseCurrentX += x * deltaTime;
	else if (mouseCurrentX < 0) mouseCurrentX = 0;
	else if (mouseCurrentX > WINDOW_WIDTH) mouseCurrentX = WINDOW_WIDTH;
	if (mouseCurrentY > 0 && mouseCurrentY < WINDOW_HEIGHT) mouseCurrentY += y * deltaTime;
	else if (mouseCurrentY < 0) mouseCurrentY = 0;
	else if (mouseCurrentY > WINDOW_HEIGHT) mouseCurrentY = WINDOW_HEIGHT;
	*/
}